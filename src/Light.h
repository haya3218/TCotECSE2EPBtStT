// Custom light code, inspired by Red Sands/MOD.
// NOT decompiled code
//
// This code is licensed under the MIT License.
//
// Copyright (c) 2024 haya idk
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#pragma once

#include "WindowsWrapper.h"
#include "CommonDefines.h"

#include <vector>
#define LIGHT_MAX 0x200

#include "Map.h"

typedef struct light_s {
	int x, y; // coordinates
	int w, h; // scale of the light
	unsigned long col; // RGBA value for the colors
	int num; // Internal number for reference
	BOOL marked; // yeah
	int cond;
} light_t; // yes, this is doom type syntax. no im not making this a class, it isnt necessary.

extern light_t gLIT[LIGHT_MAX];
extern unsigned char light_level; // controls the actual light level

// Functions
// Init
void InitLights(void);
// Adding lights
light_t* AddLight(int num, int x, int y, int w, int h, unsigned long col);
light_t* AddLight(int num, int x, int y, int w, int h);
light_t* AddLight(int num, int x, int y, int s);
// Removing lights
void RemoveLight(int num);
void RemoveLight(light_t* light);
void RemoveLight(light_t light);
// Actually render the lights
void PutLights(int fx, int fy);
// Stage lighting
void PutStage_Light(int fx, int fy);