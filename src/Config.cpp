// THIS IS DECOMPILED PROPRIETARY CODE - USE AT YOUR OWN RISK.
//
// The original code belongs to Daisuke "Pixel" Amaya.
//
// Modifications and custom code are under the MIT licence.
// See LICENCE.txt for details.

#include <stddef.h>
#include <stdio.h>
#include <string.h>
#include <string>

#include "WindowsWrapper.h"

#include "Backends/Misc.h"
#include "Config.h"
#include "File.h"
#include "Main.h"
#include "Game.h"

#include "mini/ini.h"

#include <fstream>
#include <vector>
#include <map>

const char* const gConfigName = "Config.ini";
const char* const gProof = "CSE2E   20200430";

static std::vector<std::string> binding_names = {
	"up",
	"down",
	"left",
	"right",
	"ok",
	"cancel",
	"jump",
	"shoot",
	"arms_rev",
	"arms",
	"items",
	"map",
	"pause"
};

static std::unordered_map<std::string, int> display_modes = {
	{"Fullscreen", 	0},
	{"320x240", 	1},
	{"640x480", 	2},
	{"960x720", 	3},
	{"1280x960", 	4},
};
// i am too lazy to come up with a solution so heres a dumb thing instead
static std::vector<std::string> display_mode_lookup = {
	"Fullscreen", "320x240", "640x480", "960x720", "1280x960"
};

inline const char * const BoolToString(BOOL b)
{
	return b ? "1" : "0";
}
inline BOOL const StringToBool(std::string str)
{
	return (std::stoi(str) == 1);
}

BOOL LoadConfigData(CONFIGDATA *conf)
{
	// Clear old configuration data
	// Kinda odd seeing C-style code with the modern C++ code below, ain't it?
	memset(conf, 0, sizeof(CONFIGDATA));

	std::string path = gModulePath + '/' + gConfigName;

	mINI::INIFile file(path);
	mINI::INIStructure ini;

	file.read(ini);
	auto config = ini.get("config");

	if (!ini.has("config"))
		return FALSE;

	conf->font_name = config.get("FontName").c_str();

	auto it = display_modes.find(config["DisplayMode"]);
	if (it != display_modes.end())
		conf->display_mode = display_modes.at(config["DisplayMode"]);

	if (config.has("60fps"))
		conf->b60fps = StringToBool(config.get("60fps"));

	if (config.has("VSync"))
		conf->bVsync = StringToBool(config.get("VSync"));

	if (config.has("DynamicLights"))
		conf->bFancyLighting = StringToBool(config.get("DynamicLights"));

	if (config.has("StageLights"))
		conf->bFancierLighting = StringToBool(config.get("StageLights"));

	auto keyboard = ini.get("keyboard");
	auto controller = ini.get("controller");

	for (size_t i = 0; i < BINDING_TOTAL; ++i)
	{
		if (ini.has("keyboard"))
			conf->bindings[i].keyboard = std::stoi(keyboard.get(binding_names[i]));
		if (ini.has("controller"))
			conf->bindings[i].controller = std::stoi(controller.get(binding_names[i]));
	}

	if (ini.has("seeecret"))
	{
		auto seeecret = ini.get("seeecret");
		if (seeecret.has("billy"))
			bBESTVALUE = StringToBool(seeecret["billy"]);
		else if (seeecret.has("DebugMode")) // alt name
			bBESTVALUE = StringToBool(seeecret["DebugMode"]);
		if (seeecret.has("SpeedrunMode"))
			bSpeedrunMode = StringToBool(seeecret["SpeedrunMode"]);
	}

	return TRUE;
}

BOOL SaveConfigData(const CONFIGDATA *conf)
{
	// Get path
	std::string path = gModulePath + '/' + gConfigName;

	mINI::INIFile file(path);
	mINI::INIStructure ini;

	file.read(ini);

	auto& config = ini["config"]; // doesn't exist, meaning it'll create one
	config["DisplayMode"] = display_mode_lookup[conf->display_mode];
	config["60fps"] = BoolToString(conf->b60fps);
	config["VSync"] = BoolToString(conf->bVsync);

	auto& keyboard = ini["keyboard"];
	auto& controller = ini["controller"];

	for (size_t i = 0; i < BINDING_TOTAL; ++i)
	{
		ini["keyboard"][binding_names[i]] = std::to_string(conf->bindings[i].keyboard);
		controller[binding_names[i]] = std::to_string(conf->bindings[i].controller);
	}

	file.write(ini);

	return TRUE;
}

void DefaultConfigData(CONFIGDATA *conf)
{
	// Clear old configuration data
	memset(conf, 0, sizeof(CONFIGDATA));

	strcpy(conf->proof, gProof);

	// Fun fact: The Linux port added this line:
	// conf->display_mode = 1;

#ifdef _3DS
	conf->display_mode = 1;
#elif defined(__riscos__)
	conf->display_mode = 2;
#endif

	// Defaults since I'm a bitch
	conf->display_mode = 2;
	conf->b60fps = TRUE;
	conf->bFancyLighting = FALSE;
	conf->bFancierLighting = FALSE;

	// Reset joystick settings (as these can't simply be set to 0)
	conf->bindings[BINDING_UP].controller = 7;
	conf->bindings[BINDING_DOWN].controller = 8;
	conf->bindings[BINDING_LEFT].controller = 9;
	conf->bindings[BINDING_RIGHT].controller = 10;
	conf->bindings[BINDING_OK].controller = 1;
	conf->bindings[BINDING_CANCEL].controller = 0;
	conf->bindings[BINDING_JUMP].controller = 1;
	conf->bindings[BINDING_SHOT].controller = 0;
	conf->bindings[BINDING_ARMSREV].controller = 3;
	conf->bindings[BINDING_ARMS].controller = 4;
	conf->bindings[BINDING_ITEM].controller = 5;
	conf->bindings[BINDING_MAP].controller = 2;
	conf->bindings[BINDING_PAUSE].controller = 6;

	// Set default key bindings
	conf->bindings[BINDING_UP].keyboard = BACKEND_KEYBOARD_UP;
	conf->bindings[BINDING_DOWN].keyboard = BACKEND_KEYBOARD_DOWN;
	conf->bindings[BINDING_LEFT].keyboard = BACKEND_KEYBOARD_LEFT;
	conf->bindings[BINDING_RIGHT].keyboard = BACKEND_KEYBOARD_RIGHT;
	conf->bindings[BINDING_OK].keyboard = BACKEND_KEYBOARD_Z;
	conf->bindings[BINDING_CANCEL].keyboard = BACKEND_KEYBOARD_X;
	conf->bindings[BINDING_JUMP].keyboard = BACKEND_KEYBOARD_Z;
	conf->bindings[BINDING_SHOT].keyboard = BACKEND_KEYBOARD_X;
	conf->bindings[BINDING_ARMSREV].keyboard = BACKEND_KEYBOARD_A;
	conf->bindings[BINDING_ARMS].keyboard = BACKEND_KEYBOARD_S;
	conf->bindings[BINDING_ITEM].keyboard = BACKEND_KEYBOARD_Q;
	conf->bindings[BINDING_MAP].keyboard = BACKEND_KEYBOARD_W;
	conf->bindings[BINDING_PAUSE].keyboard = BACKEND_KEYBOARD_ESCAPE;
}
