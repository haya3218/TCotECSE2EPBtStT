// Custom light code, inspired by Red Sands/MOD.
// NOT decompiled code
//
// This code is licensed under the MIT License. (see Light.h).

#include "Light.h"
#include "Draw.h"

#include <algorithm>
#include "NpChar.h"
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <memory.h>

light_t gLIT[LIGHT_MAX]; // light sources
static BOOL init = false;
unsigned char light_level;

void InitLights(void)
{
	// clear out all possible lights first
	memset(gLIT, 0, sizeof(gLIT));
	init = true; // dumb hack to prevent crash
	light_level = 0x00;
}

// Internal function for main function to reference
static light_t* AddLightInternal(int num, int x, int y, int w, int h, unsigned long col)
{
	int n;
	for (n = 0; n < NPC_MAX; n++)
	{
		if (!gLIT[n].cond)
		{
			//Set LIGHT parameters
			memset(&gLIT[n], 0, sizeof(light_t));
			gLIT[n].cond |= 0x80u;
			gLIT[n].num = num;
			gLIT[n].x = x;
			gLIT[n].y = y;
			gLIT[n].w = w;
			gLIT[n].h = h;
			gLIT[n].col = col;
			gLIT[n].marked = FALSE;
			break;
		}
	}
	return &gLIT[n];
}

light_t* AddLight(int num, int x, int y, int w, int h, unsigned long col)
{
	return AddLightInternal(num, x, y, w, h, col);
}
light_t* AddLight(int num, int x, int y, int w, int h)
{
	return AddLightInternal(num, x, y, w, h, RGBA(0xFF, 0xFF, 0xFF, 0xFF));
}
light_t* AddLight(int num, int x, int y, int s)
{
	return AddLightInternal(num, x, y, s, s, RGBA(0xFF, 0xFF, 0xFF, 0xFF));
}

// Remove light source
void RemoveLight(int num)
{
	auto find_cond = [num](light_t light) { return light.num == num; };
	
}

void RemoveLight(light_t* light)
{
	// mark for deletion
	light->marked = TRUE;
}

void RemoveLight(light_t light)
{
	// mark for deletion
	light.marked = TRUE;
}

// Actually render the lights
void PutLights(int fx, int fy)
{
	// we want to clear out the light buffer before this.
	CortBox2(&grcFull, RGBA(0x00, 0x00, 0x00, light_level), SURFACE_ID_LIGHT_MAP);

	for (auto const &light : gLIT)
	{
		if (!(light.cond & 0x80) || light.marked)
			continue; // dont bother
		// first, set our color values (this will be reverted later)
		ColorMod(light.col, SURFACE_ID_LIGHT);
		// second, get the scale via making a rect
		RECT rect = {0, 0, light.w, light.h};
		// then, actually add the light via Surface2SurfaceIndirect
		Surface2SurfaceIndirect(
			SubpixelToScreenCoord(light.x) - PixelToScreenCoord(light.w / 2) - SubpixelToScreenCoord(fx),
			SubpixelToScreenCoord(light.y) - PixelToScreenCoord(light.h / 2) - SubpixelToScreenCoord(fy),
			&rect,
			SURFACE_ID_LIGHT_MAP,
			SURFACE_ID_LIGHT
		);
	}
	// render the actual lights
	PutBitmap3(&grcFull, 0, 0, &grcFull, SURFACE_ID_LIGHT_MAP);

	// remove if possible
	for(int i = NPC_MAX; 
		i>0; i--) {
		if (!(gLIT[i].cond & 0x80) || gLIT[i].marked)
			gLIT[i].cond = 0;
	}
}

void PutStage_Light(int fx, int fy)
{
	// we could just reuse transbg2 here again, since we don't really care
	CortBox2(&grcFull, RGBA(0x00, 0x00, 32, 0xFF), SURFACE_ID_LIGHT_MAP);
	
	int i, j;
	int offset;
	// Get range to draw
	int extra_tiles_drawn = 8;
	int num_x = std::min((int)gMap.width, ((WINDOW_WIDTH + (16 - 1)) / 16) + 1 + (extra_tiles_drawn*2));
	int num_y = std::min((int)gMap.length, ((WINDOW_HEIGHT + (16 - 1)) / 16) + 1 + (extra_tiles_drawn*2));
	int put_x = std::max(0, ((fx / 0x200) + 8) / 16 - extra_tiles_drawn);
	int put_y = std::max(0, ((fy / 0x200) + 8) / 16 - extra_tiles_drawn);

	int atrb;

	for (j = put_y; j < put_y + num_y; ++j)
	{
		for (i = put_x; i < put_x + num_x; ++i)
		{
			// Get attribute
			offset = (j * gMap.width) + i;
			atrb = GetAttributeOrNeg(i, j);
			if(atrb == 0 || atrb == 0x40 || atrb == 0x01 || atrb == 0x60){
				// skrrt
				int pwr = 72;
				ColorMod(RGBA(0xFF, 0xFF, 0xFF, 0xFF), SURFACE_ID_LIGHT);
				RECT rect = {0, 0, pwr * 2, pwr * 2};
				Surface2SurfaceIndirect(
					SubpixelToScreenCoord(i * 0x10 * 0x200) - PixelToScreenCoord(pwr) - SubpixelToScreenCoord(fx),
					SubpixelToScreenCoord(j * 0x10 * 0x200) - PixelToScreenCoord(pwr) - SubpixelToScreenCoord(fy),
					&rect,
					SURFACE_ID_LIGHT_MAP,
					SURFACE_ID_LIGHT
				);
			}
		}
	}
	
	PutBitmap3(&grcFull, 0, 0, &grcFull, SURFACE_ID_LIGHT_MAP);
}